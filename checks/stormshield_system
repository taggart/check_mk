#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-
# +------------------------------------------------------------------+
# |             ____ _               _        __  __ _  __           |
# |            / ___| |__   ___  ___| | __   |  \/  | |/ /           |
# |           | |   | '_ \ / _ \/ __| |/ /   | |\/| | ' /            |
# |           | |___| | | |  __/ (__|   <    | |  | | . \            |
# |            \____|_| |_|\___|\___|_|\_\___|_|  |_|_|\_\           |
# |                                                                  |
# | Copyright Mathias Kettner 2017             mk@mathias-kettner.de |
# +------------------------------------------------------------------+
#
# This file is part of Check_MK.
# The official homepage is at http://mathias-kettner.de/check_mk.
#
# check_mk is free software;  you can redistribute it and/or modify it
# under the  terms of the  GNU General Public License  as published by
# the Free Software Foundation in version 2.  check_mk is  distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY;  with-
# out even the implied warranty of  MERCHANTABILITY  or  FITNESS FOR A
# PARTICULAR PURPOSE. See the  GNU General Public License for more de-
# tails. You should have  received  a copy of the  GNU  General Public
# License along with GNU Make; see the file  COPYING.  If  not,  write
# to the Free Software Foundation, Inc., 51 Franklin St,  Fifth Floor,
# Boston, MA 02110-1301 USA.


# example output


def inventory_stormshield_system(info):
    for line in info[0]:
        yield (line[0], None)
#        return [ (line[0], None) ]


def check_stormshield_system(item, params, info):
    data = False
    for line in info[0]:
        if line[0] == item:
            data = line

    if data:
        smart = data[1]
        if smart.lower() == "passed":
            state = 0
        else:
            state = 2
        yield state, "SMART Result: %s" % smart

        if data[2] == "1":
            yield 0, "Raid Status: %s, Disk position: %s" % \
                     (data[3], data[4])



check_info['stormshield_system'] = {
    'inventory_function'    : inventory_stormshield_system,
    'check_function'        : check_stormshield_system,
    'service_description'   : 'Disk %s',
    'snmp_info'             : [ ('.1.3.6.1.4.1.11256.1.10.5.1', [
                                    '2', # STORMSHIELD-SYSTEM-MONITOR-MIB::snsDiskEntryDiskName
                                    '3', # STORMSHIELD-SYSTEM-MONITOR-MIB::snsDiskEntrySmartResult
                                    '4', # STORMSHIELD-SYSTEM-MONITOR-MIB::snsDiskEntryIsRaid
                                    '5', # STORMSHIELD-SYSTEM-MONITOR-MIB::snsDiskEntryRaidStatus
                                    '6', # STORMSHIELD-SYSTEM-MONITOR-MIB::snsDiskEntryPosition
                                ]),
                                ('.1.3.6.1.4.1.11256.1.10.7.1', [
                                    '1', # STORMSHIELD-SYSTEM-MONITOR-MIB::snsCpuIndex
                                    '2', # STORMSHIELD-SYSTEM-MONITOR-MIB::snsCpuTemp
                                ]),
                              ],
    'snmp_scan_function'    : stormshield_scan_function,
    'includes'              : [ 'stormshield.include' ],
}


def inventory_stormshield_system_cpu(info):
    for line in info[1]:
        yield (line[0], None)

def check_stormshield_system_cpu(item, params, info):
    data = False
    for line in info[1]:
        if line[0] == item:
            data = line

    if data:
        return check_temperature(int(data[1]), params, "stormshield_system_%s" % item)

check_info['stormshield_system.cpu'] = {
    'inventory_function'    : inventory_stormshield_system_cpu,
    'check_function'        : check_stormshield_system_cpu,
    'service_description'   : 'Temperature CPU %s',
    'includes'              : [ 'temperature.include' ],
    'has_perfdata'          : True,
}
